'''
3 3
3 1 2
4 1 4
2 2 2

2 4
7 3 1 8
3 3 3 4
'''

n, m = map(int, input().split())
data = []
a =[]
for i in range(n):
	data.append(list(map(int, input().split())))
	data[i].sort()
	a.append(data[i][0])
print(max(a))