'''
a1

2


c2


6
'''
current = input()
current_column_text = current[0]
column = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
row = [i for i in range(8)]
# row =[0, 1, 2, 3, 4, 5, 6, 7]

index = 0
# 현재 위치 문자열을 인덱스로 변환
for i, n in enumerate(column):
	if current[0] == n:
		index = i

current_row = current[1]

x = [-2, -2, -1, 1, 2, 2, -1, 1]
y = [-1, 1, 2, 2, -1, 1, -2, -2]

current_x = int(current[1])
current_y = int(index + 1)

count = 0
for i in range(8):
	if current_x + x[i] < 1 or current_y + y[i] < 1 or current_x + x[i] > 8 or current_y + y[i] > 8:
		continue
	count += 1

print(count)
