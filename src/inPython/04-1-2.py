n = int(input())
direction = input().split()

x, y = 1, 1
lx = [0, -1, 0, 1]
ly = [-1, 0, 1, 0]
dir = ['L', 'U', 'R', 'D']

for i in direction:
	for j in range(len(dir)):
		if i == dir[j]:
			if x + lx[j] < 1 or y + ly[j] < 1 or x + lx[j] > n or y + ly[j] > n:
				continue
			x += lx[j]
			y += ly[j]

print(x, y)