'''
4 4
1 1 0
1 1 1 1
1 0 0 1
1 1 0 1
1 1 1 1
'''

n, m = map(int, input().split())
x, y, direction = map(int, input().split())
array = [[0 for i in range(n)] for j in range(m)]
print()

# game_map : 받은 맵
game_map = []
for i in range(n):
	data = list(map(int, input().split()))
	game_map.append(data)

# array : 방문한 곳 체크
array[x][y] = 1
print(array)
print(game_map)

dir_x = [-1, 0, 1, 0]
dir_y = [0, 1, 0, -1]

to = [3, 2, 1, 0]
count = 1


def turn_left():
	global direction
	direction -= 1
	if direction == -1:
		direction = 3


turn_time = 0
while True:
	# 왼쪽으로 회전
	turn_left()

	temp_x = x + dir_x[direction]
	temp_y = y + dir_y[direction]

	if game_map[temp_x][temp_y] is 0 and array[temp_x][temp_y] is not 1:
		x = temp_x
		y = temp_y
		count += 1
		array[temp_x][temp_y] = 1
		turn_time = 0
		continue
	else:
		turn_time += 1

	if turn_time == 4:
		temp_x = x - dir_x[direction]
		temp_y = y - dir_y[direction]

		if array[temp_x][temp_y] == 0:
			x = temp_x
			y = temp_y
		else:
			break
		turn_time = 0

print(count)
# # 현재 위치 현재 방향에서 갈 곳을 측정함
# for i in to:
# 	# 갈 수 있는 곳인지 체크
# 	# 인덱스 0보다 크고, 맵상의 바다가 아니고, 지나간곳이 아니면 이동
# 	temp_x = x + dir_x[i]
# 	temp_y = y + dir_y[i]
# 	if temp_x > 0 and temp_y > 0 and (game_map[temp_x][temp_y] is not 1) and (array[temp_x][temp_y] is not 1):
# 		x = temp_x
# 		y = temp_y
# 		array[x][y] = 1
# 		count += 1
# 		dir = i
#
# if x + dir_x[dir] > 0 and y + dir_y[dir] > 0 and array[x + dir_x[dir]][y + dir_y[dir]] is 1:
# 	x = x - dir_x[dir]
# 	y = y - dir_y[dir]
#
# 	if game_map[x][y] == 1:
# 		break

