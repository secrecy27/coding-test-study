'''
00시 00분 00초부터 N시 59분 59초 까지 주어진 값 N이 포함되는 모든 경우의 수
5

11475
'''

n = int(input())

# hour 0 ~ n
# time 0 ~ 59
# second 0 ~ 59
count = 0

for i in range(n + 1):
	for j in range(60):
		for k in range(60):
			if '3' in str(i) + str(j) + str(k):
				count += 1

print(count)
