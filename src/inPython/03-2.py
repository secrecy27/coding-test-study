# 5 8 3
# 2 4 5 4 6

import time

# 숫자 일렬로 받기
n, m, k = map(int, input().split())
data = list(map(int, input().split()))
start_time = time.time()
first = max(data)
data.remove(first)
second = max(data)

print(first)
print(second)


def sum(m, k, first, second):
	count = 1
	returnValue = 0
	initialK = k
	countK = k
	for i in range(1, m + 1):
		if count <= countK:
			returnValue += first
			count += 1
			# print("1 ", i)
		elif count > countK:
			returnValue += second
			countK = initialK
			count = 1
			# print("2 ", i)
	return returnValue


# print("===")
print(sum(m, k, first, second))

end_time = time.time()
print("시간 : ", end_time-start_time)