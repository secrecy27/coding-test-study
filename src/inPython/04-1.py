'''
예제1)
5
R R R U D D

답1)
3 4
---------
예제2)
5
R U U R R R R

답2)
1 5
'''

n = int(input())
direction = input().split()

lx = [0, -1, 0, 1]
ly = [-1, 0, 1, 0]
dir = ['L', 'U', 'R', 'D']

x, y = 1, 1
temp_x, temp_y = 1, 1
for i in direction:
	for j in range(len(dir)):
		if i == dir[j]:
			temp_x = x + lx[j]
			temp_y = y + ly[j]

	if temp_x <= 0 or temp_y <= 0 or temp_x > n or temp_y > n:
		continue
	x = temp_x
	y = temp_y

print('====')
print(x, y)
