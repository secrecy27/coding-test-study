package basic;

import java.util.Scanner;

// 재귀함수 분석(스택을 이용하는 재귀)
public class basic56 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int value = s.nextInt();
        D(value);
    }

    private static void D(int value) {
        if (value == 0) {
            return;
        }
        int val = value - 1;
        D(val);
        System.out.print(value + " ");
    }

}

