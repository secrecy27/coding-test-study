package basic;

import java.util.ArrayList;
import java.util.Scanner;

// merge sort
// 8
// 7 6 3 1 5 2 4 8
public class basic62 {
    public static int[] arr;
    public static int[] temp;
    public static int length = 0;
    public static int index = 0;

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        length = s.nextInt();
        arr = new int[length];
        temp = new int[length];
        for (int i = 0; i < length; i++) {
            arr[i] = s.nextInt();
        }
        mergeSort(arr, 0, length - 1);

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]);
        }
    }

    public static void merge(int[] array, int left, int mid, int right) {
        int i = left;
        int j = mid + 1;
        int k = left;
        int l;

        // merge
        while (i <= mid && j <= right) {
            if (array[i] < array[j]) {
                temp[k++] = array[i++];
            } else {
                temp[k++] = array[j++];
            }
        }

        if (i > mid) {
            for (l = j; l <= right; l++) {
                temp[k++] = array[l];
            }
        } else {
            for (l = i; l <= mid; l++) {
                temp[k++] = array[l];
            }
        }

        for (l = left; l <= right; l++) {
            array[l] = temp[l];
        }
    }

    public static void mergeSort(int[] array, int left, int right) {
        if (left < right) {
            int mid = (left + right) / 2;
            //divide
            mergeSort(array, left, mid);
            mergeSort(array, mid + 1, right);

            //conquer
            merge(array, left, mid, right);
        }
    }


}
