package basic;

import java.util.Arrays;
import java.util.Scanner;

public class basic60 {
    public static int number;
    public static int[] arr;
    public static int total = 0;
    public static boolean flag = false;

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Scanner s2 = new Scanner(System.in);
        number = s.nextInt();
        arr = new int[number];
        for (int i = 0; i < number; i++) {
            int temp = s2.nextInt();
            arr[i] = temp;
            total += temp;
        }
        System.out.println(number);
        System.out.println(Arrays.toString(arr));

        DFS(1, 0);
        if (flag) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }

    public static void DFS(int L, int sum) {
        if (sum > (total / 2)) return;
        if (flag) return;
        if (L == number + 1) {
            if (sum == (total - sum)) {
                flag = true;
            }
        } else {
            DFS(L + 1, sum + arr[L - 1]);
            DFS(L + 1, sum);
        }
    }
}
