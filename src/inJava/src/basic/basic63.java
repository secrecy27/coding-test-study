package basic;

import java.util.Arrays;
import java.util.Scanner;

//인접 행렬
//6 9
//1 2 7
//1 3 4
//2 1 2
//2 3 5
//2 5 5
//3 4 5
//4 2 2
//4 5 5
//6 4 5
public class basic63 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int node = s.nextInt();
        int line = s.nextInt();
        int[][] arr = new int[node][node];

        for (int i = 0; i < line; i++) {
            int a = s.nextInt();
            int b = s.nextInt();
            int c = s.nextInt();
            arr[a-1][b-1] = c;
        }

        for (int i = 0; i < node; i++) {
            for (int j = 0; j < node; j++) {
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
    }

}
