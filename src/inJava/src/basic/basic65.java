package basic;

import java.util.Scanner;

// 미로탐색(DFS)
//0 0 0 0 0 0 0
//0 1 1 1 1 1 0
//0 0 0 1 0 0 0
//1 1 0 1 0 1 1
//1 1 0 0 0 0 1
//1 1 0 1 1 0 0
//1 0 0 0 0 0 0

public class basic65 {
    public static int[][] arr = new int[8][8];
    public static int[][] check = new int[8][8];
    public static int count = 0;
    public static int[] a = {0, 1, 0, -1};
    public static int[] b = {1, 0, -1, 0};

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        for (int i = 1; i < 8; i++) {
            for (int j = 1; j < 8; j++) {
                int value = s.nextInt();
                arr[i][j] = value;
                check[i][j] = value;

            }
        }
//        for (int i = 1; i < 8; i++) {
//            for (int j = 1; j < 8; j++) {
//                System.out.print(arr[i][j]);
//            }
//            System.out.println();
//        }

        check[1][1] = 1;
        DFS(1, 1);
        System.out.println("총 횟수 : " + count);
    }

    public static void DFS(int x, int y) {
        if (x == 7 && y == 7) {
            count++;
        } else {
            for (int i = 0; i < 4; i++) {
                int xx = x + a[i];
                int yy = y + b[i];
                if (xx > 7 || xx < 1 || yy > 7 || yy < 1) {
                    continue;
                }
                if (arr[xx][yy] == 0 && check[xx][yy] == 0) {
                    check[xx][yy] = 1;
                    DFS(xx, yy);
                    check[xx][yy] = 0;
                }
            }
        }
    }

}
