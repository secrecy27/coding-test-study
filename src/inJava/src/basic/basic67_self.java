package basic;

import java.util.Scanner;

//5 8
//1 2 12
//1 3 6
//1 4 10
//2 3 2
//2 5 2
//3 4 3
//4 2 2
//4 5 5

// 최소비용 DFS(인접행렬)
public class basic67_self {
    public static int n = 0;
    public static int[][] arr;
    public static int[] temp;
    public static int oldSum = 0;
    public static int[] newSum;

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        n = s.nextInt();
        int M = s.nextInt();
        arr = new int[n + 1][n + 1];
        temp = new int[n + 1];
        newSum = new int[n + 1];

        for (int i = 0; i < M; i++) {
            int a = s.nextInt();
            int b = s.nextInt();
            int c = s.nextInt();
            arr[a][b] = c;
        }
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                System.out.print(arr[i][j] + "  ");
            }
            System.out.println();
        }
        temp[1] = 1;
        DFS(1);
        System.out.println(oldSum);
    }

    public static void DFS(int v) {
        if (v == n) {
            int sum = 0;
            for (int i = 1; i <= n; i++) {
                sum += newSum[i];
            }

            if (oldSum == 0 || oldSum >= sum) {
                oldSum = sum;
            }
        } else {
            for (int i = 1; i <= n; i++) {
                if (arr[v][i] >= 1 && temp[i] == 0) {
                    temp[i] = 1;
                    newSum[i] = arr[v][i];
                    DFS(i);
                    temp[i] = 0;
                    newSum[i] = 0;
                }
            }
        }
    }
}
