package basic;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

// 파일 입출력 버전
public class basic5_2 {
    public static void main(String[] args) {
        try {
            String inputFilePath = "src/inJava/src/basic/input";
            String outputFilePath = "src/inJava/src/basic/output";
            String inputFileName = "/inputForBasic5_2.txt";
            String outputFileName = "/outputForBasic5_2.txt";

            String gender = "W";
            BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFilePath + inputFileName));
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFilePath + outputFileName));
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                String two = line.substring(0, 2);
                int sevenNumber = Integer.parseInt(line.substring(7, 8));
                int nowYear = 2019;

                int changeYear = 0;
                if (sevenNumber == 1 || sevenNumber == 2) {
                    // 1900년생
                    changeYear = Integer.parseInt("19" + two);
                } else if (sevenNumber == 3 || sevenNumber == 4) {
                    // 2000년생
                    changeYear = Integer.parseInt("20" + two);
                }
                int age = nowYear - changeYear + 1;

                if (sevenNumber == 1 || sevenNumber == 3) {
                    gender = "M";
                } else if (sevenNumber == 2 || sevenNumber == 4) {
                    gender = "W";
                }
                bufferedWriter.write(age + " " + gender);
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
