package basic;

import java.io.*;

public class basic4 {
    public static void main(String[] args) {
        try {
            String inputFilePath = "src/inJava/src/basic/input";
            String outputFilePath = "src/inJava/src/basic/output";
            String inputFileName = "/inputForBasic4.txt";
            String outputFileName = "/outputForBasic4.txt";
            BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFilePath + inputFileName));
            int number = Integer.parseInt(bufferedReader.readLine());
            String[] value = bufferedReader.readLine().split(" ");
            int min = 2147000000;
            int max = -2147000000;
            for (int i = 0; i < number; i++) {
                int data = Integer.parseInt(value[i]);
                if (data > max) max = data;
                if (data < min) min = data;
            }
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFilePath + outputFileName));
            bufferedWriter.write(String.valueOf(max - min));
            bufferedWriter.flush();
            bufferedWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
