package basic;

import java.util.Scanner;

//9 7
//1 2
//2 3
//3 4
//4 5
//6 7
//7 8
//8 9
//3 8 > NO   1 2 > YES
//disjoint set : Union & Find 알고리즘
public class basic77 {
    public static int[] arr;

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n, m, a, b;
        n = s.nextInt();
        m = s.nextInt();
        arr = new int[n + 1];
        for (int i = 1; i <= n; i++) {
            arr[i] = i;
        }
        for (int i = 1; i <= m; i++) {
            a = s.nextInt();
            b = s.nextInt();
            Union(a, b);
        }
        a = s.nextInt();
        b = s.nextInt();
        a = Find(a);
        b = Find(b);
        if (a == b) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }

    public static int Find(int v) {
        if (v == arr[v]) {
            return v;
        } else {
            return arr[v] = Find(arr[v]);
        }
    }

    public static void Union(int a, int b) {
        a = Find(a);
        b = Find(b);
        if (a != b) {
            arr[a] = b;
        }
    }
}
