package basic;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

//8 3
//공주 구하기
public class basic72 {
    public static int N = 0;
    public static int K = 0;

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        N = s.nextInt();
        K = s.nextInt();

        Queue<Integer> queue = new LinkedList<>();
        for (int i = 1; i <= N; i++) {
            queue.offer(i);
        }
        int x;
//        while (queue.size() != 1) {
//            for (int i = 1; i <= K; i++) {
//                x = queue.poll();
//                if (i != K) {
//                    queue.add(x);
//                }
//            }
//        }
//        System.out.println(queue.poll());
        while (!queue.isEmpty()) {
            for (int i = 1; i < K; i++) {
                x = queue.poll();
                queue.add(x);
            }
            queue.poll();
            if (queue.size() == 1) {
                System.out.println(queue.poll());
            }
        }
    }
}
