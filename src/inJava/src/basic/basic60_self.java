package basic;

import java.util.Arrays;
import java.util.Scanner;

//입력값
//6
//{1,3,5,6,7,10}

//부분집합의 합이 같은 지 확인하는 함수
//{1,3,5,7} = {6,10}

//출력값
//YES

//Level(Depth)이 arr의 length+1과 같다고 생각할 것
public class basic60_self {
    public static int[] arr;
    public static int total = 0;

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Scanner s2 = new Scanner(System.in);
        int number = s.nextInt();
        arr = new int[number];
        for (int i = 0; i < number; i++) {
            int temp = s2.nextInt();
            arr[i] = temp;
            total += temp;
        }
        System.out.println(number);
        System.out.println(Arrays.toString(arr));
        System.out.println(total);

        for (int i = 1; i < number / 2; i++) {
            check(i, arr);
        }

    }

    public static void check(int num, int[] arr) {
        //num 부분 집합의 갯수
        //todo - n개의 부분 집합 갯수 추출

    }

}
