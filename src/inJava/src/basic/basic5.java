package basic;

import com.sun.jdi.CharValue;

import java.util.Scanner;

public class basic5 {
    public static void main(String[] args) {
        String gender = "W";
        Scanner s = new Scanner(System.in);
//        780316-2376152
//        061102-3575393
        String line = s.nextLine();
        String two = line.substring(0, 2);
        int sevenNumber = Integer.parseInt(line.substring(7, 8));
        int nowYear = 2019;

        int changeYear = 0;
        if (sevenNumber == 1 || sevenNumber == 2) {
            // 1900년생
            changeYear = Integer.parseInt("19" + two);
        } else if (sevenNumber == 3 || sevenNumber == 4) {
            // 2000년생
            changeYear = Integer.parseInt("20" + two);
        }
        int age = nowYear - changeYear + 1;

        if (sevenNumber == 1 || sevenNumber == 3) {
            gender = "M";
        }

        System.out.println(age + " " + gender);
    }
}
