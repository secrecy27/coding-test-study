package basic;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

//5 14
//BFS - 송아지 찾기
public class basic71 {
    public static int initLocation = 0;
    public static int targetLocation = 0;
    public static int[] check;
    public static int[] distance = {1, -1, 5};

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        initLocation = s.nextInt();
        targetLocation = s.nextInt();
        check = new int[10001];
        int x;

        check[initLocation] = 1;
        Queue<Integer> queue = new LinkedList<>();
        queue.add(initLocation);

        while (!queue.isEmpty()) {
            x = queue.poll();

            for (int i = 0; i < distance.length; i++) {
                int position = x + distance[i];
                if (position < -1 || position > 10000) continue;
                if (position == targetLocation) {
                    //종료
                    System.out.println(check[x]);
                    return;
                }

                if (check[position] == 0) {
                    check[position] = check[x] + 1;
                    queue.offer(position);
                }
            }
        }
    }
}
