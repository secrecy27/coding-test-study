package basic;

import java.util.Scanner;

// 자연수 A부터가 B까지의 합
public class basic2 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int first = s.nextInt();
        int second = s.nextInt();
        int sum = 0;
        for (int i = first; i <= second; i++) {
            System.out.print(i);
            sum += i;
            if (i != second) {
                System.out.print("+");
            } else {
                System.out.print("=");
                System.out.print(sum);
            }
        }

//        for (int i = first; i < second; i++) {
//            System.out.print(i + "+");
//            sum += i;
//        }
//        System.out.print(second + "=");
//        System.out.print(sum + second);
    }
}
