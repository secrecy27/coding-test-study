package basic;
//5 8
//1 2 12
//1 3 6
//1 4 10
//2 3 2
//2 5 2
//3 4 3
//4 2 2
//4 5 5

import java.util.Scanner;

// 최소비용 DFS(인접행렬)
public class basic67 {
    public static int[][] arr;
    public static int[] temp;
    public static int n = 0;
    public static int cost = 2147000000;

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        n = s.nextInt();
        int M = s.nextInt();
        arr = new int[n + 1][n + 1];
        temp = new int[n + 1];

        for (int i = 0; i < M; i++) {
            int a = s.nextInt();
            int b = s.nextInt();
            int c = s.nextInt();
            arr[a][b] = c;
        }

        temp[1] = 1;
        DFS(1, 0);
        System.out.println(cost);
    }

    public static void DFS(int v, int sum) {
        if (v == n) {
            if (cost >= sum) {
                cost = sum;
            }
        } else {
            for (int i = 1; i <= n; i++) {
                if (arr[v][i] > 0 && temp[i] == 0) {
                    temp[i] = 1;
                    DFS(i, sum + arr[v][i]);
                    temp[i] = 0;
                }
            }
        }
    }
}
