package basic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Scanner;

//6
//50 2
//20 1
//40 2
//60 3
//30 3
//30 1

public class basic75 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int corporate = s.nextInt();
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(Collections.reverseOrder());
        ArrayList<Pair2> arrayList = new ArrayList<>();
        int maxDay = -1;
        int res = 0;

        for (int i = 0; i < corporate; i++) {
            int a = s.nextInt();
            int b = s.nextInt();
            Pair2 pair = new Pair2(a, b);
            if (b > maxDay) {
                maxDay = b;
            }
            arrayList.add(pair);
        }
        System.out.println(maxDay);

        for (int i = maxDay; i >= 1; i--) {
            for (int j = 0; j < corporate; j++) {
                if (arrayList.get(j).getB() < i) break;
                priorityQueue.add(arrayList.get(j).getA());
            }

            if (!priorityQueue.isEmpty()) {
                res += priorityQueue.poll();
            }
        }
        System.out.println(res);


    }
}

class Pair2 {
    Integer a;
    Integer b;

    public Pair2(Integer a, Integer b) {
        this.a = a;
        this.b = b;
    }

    public Integer getA() {
        return a;
    }

    public Integer getB() {
        return b;
    }


}

