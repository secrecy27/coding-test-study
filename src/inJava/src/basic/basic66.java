package basic;

import java.util.ArrayList;
import java.util.Scanner;

//5 9
//1 2
//1 3
//1 4
//2 1
//2 3
//2 5
//3 4
//4 2
//4 5
public class basic66 {
    public static int n = 0;
    public static int M = 0;
    public static ArrayList<ArrayList<Integer>> list = new ArrayList<ArrayList<Integer>>();
    public static int count = 0;
    public static int[] check;

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        n = s.nextInt();
        M = s.nextInt();
        list = initialList(n);
        check = new int[n + 1];

        for (int i = 0; i < M; i++) {
            int a = s.nextInt();
            int b = s.nextInt();
            putSingle(a, b);
        }
        System.out.println("list:" + list);
        check[1] = 1;
        DFS(1);
        System.out.println(count);
    }

    public static void DFS(int v) {
        if (v == n) {
            count++;
        } else {
            for (int i = 0; i < list.get(v).size(); i++) {
                if (check[list.get(v).get(i)] == 0) {
                    check[list.get(v).get(i)] = 1;
                    DFS(list.get(v).get(i));
                    check[list.get(v).get(i)] = 0;
                }
            }
        }
    }

    //리스트 초기화
    public static ArrayList<ArrayList<Integer>> initialList(int size) {
        for (int i = 0; i < size + 1; i++) {
            list.add(new ArrayList<>(size));
        }
        return list;
    }

    //특정 노드 리턴
    public static ArrayList<Integer> getNode(int i) {
        return list.get(i);
    }

    //그래프 추가 양방향
    public static void put(int x, int y) {
        list.get(x).add(y);
        list.get(y).add(x);
    }

    //그래프 추가 단방향
    public static void putSingle(int x, int y) {
        list.get(x).add(y);
    }
}
