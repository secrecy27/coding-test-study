package basic;


import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Scanner;

//5
//3
//6
//0
//5
//0
//2
//4
//0
//-1
//최대 힙 - 우선순위 큐
public class basic73 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        //기본은 우선순위가 낮은 숫자
        //Collections.reverseOrder() 사용시 높은 숫자 우선순위
        PriorityQueue<Integer> priority = new PriorityQueue<>(Collections.reverseOrder());

        while (true) {
            int value = s.nextInt();
            if (value == -1) {
                break;
            }
            if (value == 0) {
                if (priority.isEmpty()) {
                    System.out.println("-1");
                } else {
                    System.out.println(priority.poll());
                }
            } else {
                priority.offer(value);
            }

        }
    }
}
