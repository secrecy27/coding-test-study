package basic;

import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Scanner;

//5
//3
//6
//0
//5
//0
//2
//4
//0
//-1
//최소힙 - 우선순위 큐
public class basic74 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(Collections.reverseOrder());
        while (true) {
            int value = s.nextInt();
            if (value == -1) {
                break;
            }
            if (value == 0) {
                System.out.println(-1*priorityQueue.poll());
            } else {
                priorityQueue.offer(-1*value);
            }

        }
    }
}
