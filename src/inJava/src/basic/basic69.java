package basic;

import java.util.ArrayList;
import java.util.Scanner;


//1 2
//1 3
//2 4
//2 5
//3 6
//3 7
//BFS (넓이우선탐색) - 이진트리
public class basic69 {
    public static int[] Q = new int[100];
    public static int front = -1;
    public static int back = -1;
    public static int[] ch = new int[10];
    public static ArrayList<ArrayList<Integer>> arrayList = new ArrayList<ArrayList<Integer>>(10);

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        initialArrayList(10);
        int x;
        for (int i = 1; i <= 6; i++) {
            int a = s.nextInt();
            int b = s.nextInt();
            //put(a,b) - 양방향
            arrayList.get(a).add(b);
            arrayList.get(b).add(a);

        }
        System.out.println(arrayList);
        Q[++back] = 1;
        ch[1] = 1;
        while (front < back) {
            x = Q[++front]; // front back이 같아짐
            System.out.printf("%d ", x);
            for (int i = 0; i < arrayList.get(x).size(); i++) {
                if (ch[arrayList.get(x).get(i)] == 0) {
                    ch[arrayList.get(x).get(i)] = 1;
                    Q[++back] = arrayList.get(x).get(i);
                }
            }
        }
    }

    public static void initialArrayList(int size) {
        for (int i = 0; i < size + 1; i++) {
            arrayList.add(new ArrayList<>(size));
        }
    }

    public static void put(int x, int y) {
        arrayList.get(x).add(y);
        arrayList.get(y).add(x);
    }

    public static void putSingle(int x, int y) {
        arrayList.get(x).add(y);
    }
}
