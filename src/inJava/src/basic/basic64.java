package basic;

import java.util.Arrays;
import java.util.Scanner;

//DFS(경로탐색)
//5 9
//1 2
//1 3
//1 4
//2 1
//2 3
//2 5
//3 4
//4 2
//4 5
//todo - 경로도 표시하기
public class basic64 {
    public static int node = 0;
    public static int[][] arr;
    public static int[] check;  //방문한 노드 체크할 배열
    public static int count;


    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        node = s.nextInt();
        int line = s.nextInt();
        arr = new int[node + 1][node + 1];
        check = new int[node + 1];

        for (int i = 1; i <= line; i++) {
            int a = s.nextInt();
            int b = s.nextInt();
            arr[a][b] = 1;
        }

//        for (int i = 1; i <= node; i++) {
//            for (int j = 1; j <= node; j++) {
//                System.out.print(arr[i][j]);
//            }
//            System.out.println();
//        }

        //1번 노드는 처음에 미리 방문
        check[1] = 1;
        DFS(1);
        System.out.println("총 " + count + "회");
    }

    public static void DFS(int v) {
        int i;
        if (v == node) {
            count++;
        } else {
            //1번에서 node로 이동할 수 있는 지 완전 탐색
            for (i = 1; i <= node; i++) {
                if (arr[v][i] == 1 && check[i] == 0) {
                    //v에서 i로 이동할 수 있고, i를 방문한 적이 없다면
                    check[i] = 1;
                    DFS(i);
                    check[i] = 0; //해당 라인이 실행될 때는 count가 끝난 이후로 초기화 시켜줌
                }
            }
        }
    }
}
