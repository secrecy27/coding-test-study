package basic;

import java.util.Scanner;

// 재귀함수 이진수 출력
public class basic57 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int value = s.nextInt();
        Binary(value);
    }

    public static void Binary(int value) {
        if (value == 0) {
            return;
        }
        int number = value / 2;
        int remain = value % 2;
        Binary(number);
        System.out.print(remain);
    }

}
