package basic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

//5 8
//1 2 12
//1 3 6
//1 4 10
//2 3 2
//2 5 2
//3 4 3
//4 2 2
//4 5 5
public class basic68 {
    public static int N = 0;
    public static ArrayList<ArrayList<Pair>> list = new ArrayList<>();
    public static int[] check;
    public static int cost = 2147000000;

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        N = s.nextInt();
        int M = s.nextInt();
        check = new int[N + 1];

        for (int i = 0; i <= N; i++) {
            list.add(new ArrayList<>(N + 1));
        }

        for (int i = 1; i <= M; i++) {
            int a = s.nextInt();
            int b = s.nextInt();
            int c = s.nextInt();
            Pair pair = new Pair(b, c);
            list.get(a).add(pair);
        }

        System.out.println(list);
        System.out.println(list.get(1).get(0).getValue());
        check[1] = 1;
        DFS(1, 0);
        System.out.println("최소 비용 : " + cost);

    }

    public static void DFS(int v, int sum) {
        if (v == N) {
            if (cost > sum) {
                cost = sum;
            }
        } else {
            for (int i = 0; i <list.get(v).size(); i++) {
                if (check[list.get(v).get(i).getKey()] == 0) {
                    check[list.get(v).get(i).getKey()] = 1;
                    DFS(list.get(v).get(i).getKey(), sum + list.get(v).get(i).getValue());
                    check[list.get(v).get(i).getKey()] = 0;
                }
            }
        }
    }
}

class Pair {
    Integer key;
    Integer value;

    public Pair(Integer key, Integer value) {
        this.key = key;
        this.value = value;
    }

    public Integer getKey() {
        return key;
    }

    public Integer getValue() {
        return value;
    }
}
