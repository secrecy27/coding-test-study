package basic;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

//6 9
//1 3
//1 4
//2 1
//2 5
//3 4
//4 5
//4 6
//6 2
//6 5

//BFS - 그래프 최단거리(최단거리 > 큐)
public class basic70 {
    public static int N = 0;
    public static int M = 0;
    public static ArrayList<ArrayList<Integer>> arrayList = new ArrayList<>();

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        N = s.nextInt();
        M = s.nextInt();
        int[] check = new int[N + 1];
        int[] dis = new int[N + 1];
        Queue<Integer> queue = new LinkedList<>();
        int x;

        //2차원 배열 초기화
        for (int i = 0; i < N + 1; i++) {
            arrayList.add(new ArrayList<>(N));
        }

        for (int i = 1; i <= M; i++) {
            int a = s.nextInt();
            int b = s.nextInt();
            arrayList.get(a).add(b);
        }

        queue.add(1);
        check[1] = 1;
        while (!queue.isEmpty()) {
            x = queue.poll();
            for (int i = 0; i < arrayList.get(x).size(); i++) {
                if (check[arrayList.get(x).get(i)] == 0) {
                    check[arrayList.get(x).get(i)] = 1;
                    queue.offer(arrayList.get(x).get(i));
                    dis[arrayList.get(x).get(i)] = dis[x] + 1;
                }
            }

        }
        for (int i = 2; i <= N; i++) {
            System.out.print(i + " : " + dis[i]);
            System.out.println();
        }


    }
}